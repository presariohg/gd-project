using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public class Helper {
    public static GameObject GetNearestEnemy(Transform center) {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(Constants.TAG_ENEMY);

        float minDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies) {
            float distanceToEnemy = Vector3.Distance(center.position, enemy.transform.position);
            if (distanceToEnemy < minDistance) {
                minDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        return nearestEnemy;
    }

    public static FacingSide CheckFlipSprite(GameObject self, Transform target, FacingSide facingSide) {
        bool isTargetOnTheLeft = target.position.x < self.transform.position.x;
        SpriteRenderer renderer = self.GetComponent<SpriteRenderer>();

        if (isTargetOnTheLeft) {
            if (facingSide == FacingSide.Right) {
                Helper.FlipSprite(renderer);
                return FacingSide.Left;
            }
        } else
            if (facingSide == FacingSide.Left) {
                Helper.FlipSprite(renderer);
                return FacingSide.Right;;
            }
        
        return facingSide;
    }

    public static void FlipSprite(SpriteRenderer renderer) {
        renderer.flipX = !renderer.flipX;
    }

    public static Vector3Int GetMouseTile(Grid grid) {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        mouseWorldPos[2] = 0;
        return grid.WorldToCell(mouseWorldPos);
    }

    public static bool IsTooPoor(int price) {
        return price > GameManager.instance.ManaCount;
    }

    public static bool TimeIsRunning() {
        StageManager stageManager = StageManager.instance;

        return (!stageManager.GameIsPaused &&
                !stageManager.GameIsOver &&
                 stageManager.GameIsStarted &&
                !DialogueManager.instance.ShowingDialogue);
    }

    public static int CountUnlockedEndings() {
        int ending_count = 0;
        
        string[] keys = {SettingKeys.GAME_OVER_BAD_KARMA,
                         SettingKeys.GAME_OVER_GOOD_KARMA,
                         SettingKeys.BOSS_DEFEATED_BAD_KARMA,
                         SettingKeys.BOSS_DEFEATED_GOOD_KARMA};

        foreach (string key in keys) {
            if (PlayerPrefs.GetInt(key, 0) == 1)
                ending_count++;
        }

        return ending_count;
    }
}
