using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Stage 1 wave configuration
public class Stage1Wave : WaveSpawner {
    public Transform enemySoldier;
    public Transform enemyArmored;
    public Transform enemyMounted;
    public Transform enemyBoss;

    public Transform spawnPoint;
    public TextAsset wavesJson;

    protected float countdown = 2f; // delay before starting

    void Start() {
        this.Init(this.wavesJson);
    }

    protected override void Update() {
        if (!Helper.TimeIsRunning())
            return;

        base.Update();

        if (this.wavesQueue.Count == 0) 
            return;

        if (this.countdown <= 0f) { // start a new wave
            WaveInfo currentWave = (WaveInfo) this.wavesQueue.Dequeue();

            int waveCompleted = this.totalWaves - this.wavesQueue.Count;
            this.textWaveCounter.text = waveCompleted.ToString() + "/" + this.totalWaves.ToString();

            StartCoroutine(this.SpawnWave(currentWave));
            this.countdown = currentWave.delayUntilNext;
        }

        this.countdown -= Time.deltaTime;
    }

    /**
     * Start a new wave of enemy. A wave contains a certain number of different enemy, see class WaveInfo
     * @param WaveInfo currentWave Contains info about this current wave.
     */
    override protected IEnumerator SpawnWave(WaveInfo currentWave) {
        if (!Helper.TimeIsRunning())
            yield return null;

        for(int i = 0; i < currentWave.enemyList.Length; i++) {
            Transform prefabToUse = this.enemySoldier;
            switch (currentWave.enemyList[i]) {
                case "soldier":
                    prefabToUse = this.enemySoldier;
                    break;

                case "armored":
                    prefabToUse = this.enemyArmored;
                    break;

                case "mounted":
                    prefabToUse = this.enemyMounted;
                    break;

                case "boss":
                    prefabToUse = this.enemyBoss;
                    break;
                default:
                    break;
            }

            SpawnEnemy(prefabToUse, this.spawnPoint);
            yield return new WaitForSeconds(currentWave.delayBetween);
        }
    }
}
