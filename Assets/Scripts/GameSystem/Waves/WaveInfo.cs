using System.Collections;

[System.Serializable]
public class WaveInfo {
    public float delayBetween;
    public int delayUntilNext;
    public string[] enemyList;
}
