using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

// Generic wave configuration
public abstract class WaveSpawner : MonoBehaviour {
    public Text textWaveCounter;
    protected Queue<WaveInfo> wavesQueue;
    protected int totalWaves;

    // call this in Start() of every subclass!
    protected void Init(TextAsset wavesJson) {
        StageWave waves = JsonUtility.FromJson<StageWave>(wavesJson.text);
        this.wavesQueue = new Queue<WaveInfo>(waves.wavesInfo);
        this.totalWaves = this.wavesQueue.Count;
    }

    // virtual but every other subclass must override this and call base.Upadte()
    protected virtual void Update() {
        if (!Helper.TimeIsRunning())
            return;

        if (this.wavesQueue.Count == 0) {
            GameObject[] enemiesGO = GameObject.FindGameObjectsWithTag(Constants.TAG_ENEMY);

            bool stageCleard = true;
            foreach (GameObject enemyGO in enemiesGO) {
                Enemy enemy = enemyGO.GetComponent<Enemy>();
                if (!enemy.IsFleeing) { // check if there are any advancing enemies on the field
                    stageCleard = false;
                    break;
                }
            }

            if (stageCleard)
                StageManager.instance.StageCleared();
        }

    }

    abstract protected IEnumerator SpawnWave(WaveInfo waveInfo);

    protected void SpawnEnemy(Transform enemyPrefab, Transform spawnPoint) {
        if (!Helper.TimeIsRunning())
            return;

        Instantiate(enemyPrefab, spawnPoint.position, spawnPoint.rotation);
    }
}
