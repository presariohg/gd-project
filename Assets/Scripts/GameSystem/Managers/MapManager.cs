using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using static Constants;

public class MapManager : MonoBehaviour {
    public static MapManager instance; // singleton

    public GameObject hoveringSprite;

    public bool TurretMenuClicked { get; set; }

    [SerializeField] private Grid grid;

    [SerializeField] private Tilemap buildingAllowed;
    [SerializeField] private Tilemap buildingForbidden;

    private Dictionary<Vector3, WorldTile> tiles;

    // void Awake() {
    void Awake() {
        if (instance == null) {
            this.tiles = new Dictionary<Vector3, WorldTile>();
            foreach (Vector3Int pos in this.buildingAllowed.cellBounds.allPositionsWithin) {
                var worldPosition = new Vector3Int(pos.x, pos.y, pos.z);

                if (!this.buildingAllowed.HasTile(worldPosition))
                    continue;

                WorldTile tile = (WorldTile) ScriptableObject.CreateInstance(typeof(WorldTile));

                tile.building = null;
                tile.tileBase = this.buildingAllowed.GetTile(worldPosition);
                tile.worldPosition = worldPosition;

                this.tiles.Add(tile.worldPosition, tile);
            }

            this.hoveringSprite.SetActive(false);
            instance = this;
        } else if (instance != this)
            Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update() {
        if (BuildManager.instance.TurretToBuild != null) 
            this.HoverTurretToBuild();
        
        if (Input.GetMouseButtonUp(MouseButton.LEFT))
            this.OnMouseClick();

        else if (Input.GetMouseButtonUp(MouseButton.RIGHT))
            this.OnMouseRightClick();
    }

    private void HoverTurretToBuild() {
        Vector3Int hoveredTilePos = Helper.GetMouseTile(this.grid);
        BuildManager buildManager = BuildManager.instance;
        int turretPrice = buildManager.TurretToBuild.GetComponent<Turret>().BasePrice;

        // if (!this.IsBuildable(hoveredTilePos) || Helper.IsTooPoor(turretPrice)) {
        if (!this.IsBuildable(hoveredTilePos)) {
            this.HideHoveringSprite();
            return;
        }

        WorldTile hoveredTile;

        if (this.tiles.TryGetValue(hoveredTilePos, out hoveredTile)) {
            if (hoveredTile.building == null) { 
                Vector3 hoverLocation = grid.CellToWorld(hoveredTilePos) + new Vector3(0.5f, 0.5f, 0f);    

                this.hoveringSprite.transform.position = hoverLocation; // move the hovering sprite to this tile
                this.hoveringSprite.GetComponent<SpriteRenderer>().sprite = 
                    buildManager.TurretToBuild.GetComponent<SpriteRenderer>().sprite;
                this.hoveringSprite.SetActive(true);

            } else
                this.HideHoveringSprite();
        }

    }

    private void OnMouseClick() {
        // don't build turrets if 
        if (TurretMenu.instance.IsActive()) {
            if (this.TurretMenuClicked) {
                this.TurretMenuClicked = false;
                return;
            }

            TurretMenu.instance.Hide(); // only hide when buttons on turret menu not clicked
            return;
        }

        Vector3Int clickedTilePos = Helper.GetMouseTile(this.grid);
        // TurretMenu.instance.Hide();

        if (!this.IsBuildable(clickedTilePos)) {
            TurretMenu.instance.Hide();
            return;
        }

        WorldTile clickedTile;
        if (this.tiles.TryGetValue(clickedTilePos, out clickedTile)) {
            if (clickedTile.building == null) { // empty tile, try building one here
                BuildManager buildManager = BuildManager.instance;
                Vector3 buildingLocation = grid.CellToWorld(clickedTilePos) + new Vector3(0.5f, 0.5f, 0f);    
                buildManager.Build(buildingLocation, clickedTile);

            } else {
                TurretMenu.instance.Target = clickedTile.building;
            }
        }
    }

    private void OnMouseRightClick() {
        BuildManager.instance.TurretToBuild = null;
        this.HideHoveringSprite();
    }

    private bool IsBuildable(Vector3Int tile) {
        return (buildingAllowed.HasTile(tile) && !buildingForbidden.HasTile(tile));
    }

    private void HideHoveringSprite() {
        if (this.hoveringSprite.activeSelf) 
            this.hoveringSprite.SetActive(false);
    }

}
