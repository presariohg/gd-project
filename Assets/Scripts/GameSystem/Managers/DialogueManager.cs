using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {
    public static DialogueManager instance;
    public DialogueBox dialogueBoxLeft;
    public DialogueBox dialogueBoxRight;
    public GameObject dialoguesUI;
    public bool ShowingDialogue { get { return this.isPlayingDialogue; } }

    public Dialogue[] dialogues;

    private Queue<Sentence> sentences = new Queue<Sentence>();
    private Image continueCursor;
    private DialogueBox currentDialogueBox;

    private int currentDialogueIndex;
    private bool isPlayingDialogue = false;
    private bool isTypingOneSentence = false;
    private Sentence currentSentence = null;

    void Awake () {
        if (instance != null) {
            Debug.LogError("More than one BuildManager in scene!");
            return;
        }
        instance = this;
    }

    // void Start() {
    //     this.isPlayingDialogue = 
    // }

    void Update() {
        if (!this.isPlayingDialogue)
            return;

        if (Input.GetKeyDown(KeyCode.Tab)) {
            this.FinishDialogue();
        }        
    }

    public void StartDialogue(int index) {
        this.dialoguesUI.SetActive(true);
        this.currentDialogueIndex = index;
        this.isPlayingDialogue = true;

        foreach (Sentence sentence in this.dialogues[index].sentences) {
            this.sentences.Enqueue(sentence);
        }
        this.ShowNextSentence();
    }

    public void ShowNextSentence() {
        if (this.sentences.Count == 0) {
            this.FinishDialogue();
            return;
        }

        if (this.isTypingOneSentence) {
            this.isTypingOneSentence = false;
            this.StopAllCoroutines();
            this.currentDialogueBox.sentenceContent.text = this.currentSentence.content;
            return;
        }

        Sentence sentence = this.sentences.Dequeue();
        this.currentSentence = sentence;

        if (sentence.onTheLeft) {
            this.currentDialogueBox = this.dialogueBoxLeft;
            this.dialogueBoxRight.speakerAvatar.color = Color.grey; // grey out
            this.dialogueBoxRight.gameObject.SetActive(false);
        } else {
            this.currentDialogueBox = this.dialogueBoxRight;
            this.dialogueBoxLeft.speakerAvatar.color = Color.grey; // grey out
            this.dialogueBoxLeft.gameObject.SetActive(false);
        }

        this.currentDialogueBox.gameObject.SetActive(true);
        this.currentDialogueBox.speakerName.text = sentence.speakerName;
        this.currentDialogueBox.dialogueBox.sprite = sentence.dialogueBox;
        this.currentDialogueBox.speakerAvatar.sprite = sentence.speakerAvatar;
        this.currentDialogueBox.speakerAvatar.color = Color.white;

        this.StartCoroutine(this.TypeSentence());
    }

    IEnumerator TypeSentence() {
        this.isTypingOneSentence = true;

        Text sentenceContent = this.currentDialogueBox.sentenceContent;
        sentenceContent.text = "";

        foreach (char letter in this.currentSentence.content.ToCharArray()) {
            sentenceContent.text += letter;
            yield return new WaitForSeconds(0.01f);
        }
        this.isTypingOneSentence = false;
    }

    private void FinishDialogue() {
        if (this.isPlayingDialogue) {
            this.dialoguesUI.SetActive(false);
            this.isPlayingDialogue = false;
            this.sentences = new Queue<Sentence>();
            StageManager.instance.TriggerEndDialogue(this.currentDialogueIndex);
        }
    }
}
