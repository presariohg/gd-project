using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using static Constants;

public class StageManager : MonoBehaviour {
    public static StageManager instance;

    public Toggle pauseToggle;
    public GameObject gameOverUI;
    public GameObject stageClearedUI;
    public GameObject pauseMenuUI;
    public Text textEndingsUnlocked;

    public GameObject badKarmaBlock;
    public GameObject goodKarmaBlock;

    public AudioClip gameOverSound;
    public AudioClip stageClearedSound;

    public Text textKillCount;

    public bool GameIsOver { get; private set; }
    public bool GameIsPaused { get; private set; }
    public bool GameIsStarted { get; private set; }

    public float SfxVolume {get; private set; }

    public AudioSource audioSource;


    private bool isStageCleared;
    private bool isPauseMenuOn;

    void Awake () {
        if (instance != null) {
            Debug.LogError("More than one StageManager in scene!");
            return;
        }
        instance = this;
    }

    void Start() {
        this.GameIsOver = false;
        this.GameIsPaused = false;
        this.GameIsStarted = false;
        this.isStageCleared = false;
        this.audioSource.volume = PlayerPrefs.GetFloat(SettingKeys.BGM_VOLUME, 1f);
        this.SfxVolume = PlayerPrefs.GetFloat(SettingKeys.SFX_VOLUME, 1f);

        DialogueManager.instance.StartDialogue(Stage1Dialogue.OPENING);
    }

    void Update() {
        if (Input.GetKeyUp(KeyCode.Space) && !this.pauseMenuUI.activeSelf) // only pause by space if pause menu isn't opening
            this.pauseToggle.isOn = !this.pauseToggle.isOn;
        
        if (Input.GetKeyUp(KeyCode.Escape)) {
            this.pauseToggle.isOn = !this.pauseToggle.isOn;
            this.TogglePauseMenu();
        }
    }

    public void TriggerEndDialogue(int index) {
        switch (index) {
            case (Stage1Dialogue.OPENING):
                this.GameIsStarted = true;
                break;
            case (Stage1Dialogue.GAME_OVER_GOOD_KARMA):
                PlayerPrefs.SetInt(SettingKeys.GAME_OVER_GOOD_KARMA, 1);
                this.GameIsOver = true;
                this.StartCoroutine(this.PlayAudioClip(this.gameOverSound, 0.6f));
                this.gameOverUI.SetActive(true);
                this.ShowEndingCount();
                break;
            case (Stage1Dialogue.GAME_OVER_BAD_KARMA):
                PlayerPrefs.SetInt(SettingKeys.GAME_OVER_BAD_KARMA, 1);
                this.GameIsOver = true;
                this.StartCoroutine(this.PlayAudioClip(this.gameOverSound, 0.6f));
                this.gameOverUI.SetActive(true);
                this.ShowEndingCount();
                break;

            case (Stage1Dialogue.BOSS_DEFEATED_GOOD_KARMA):
                PlayerPrefs.SetInt(SettingKeys.BOSS_DEFEATED_GOOD_KARMA, 1);
                break;

            case (Stage1Dialogue.BOSS_DEFEATED_BAD_KARMA):
                PlayerPrefs.SetInt(SettingKeys.BOSS_DEFEATED_BAD_KARMA, 1);
                break;
        }
        PlayerPrefs.Save();
    }

    public void TogglePauseGame() {
        this.GameIsPaused = this.pauseToggle.isOn;
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void TogglePauseMenu() {
        if (this.pauseToggle.isOn)
            this.pauseMenuUI.SetActive(true);
        else
            this.pauseMenuUI.SetActive(false);
    }

    public void UnPause() {
        this.pauseToggle.isOn = false;
        this.pauseMenuUI.SetActive(false);
    }

    public void GameOver() {
        if (this.GameIsOver)
            return;

        if (GameManager.instance.KillCount == 0) {
            DialogueManager.instance.StartDialogue(Stage1Dialogue.GAME_OVER_GOOD_KARMA);
        } else {
            DialogueManager.instance.StartDialogue(Stage1Dialogue.GAME_OVER_BAD_KARMA);
        }

    }

    public void MainMenu() {
        SceneManager.LoadScene("MainMenu");
    }

    public void RageQuit() {
        Application.Quit();
    }

    public void Retry() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StageCleared() {
        if (this.isStageCleared)
            return;

        StartCoroutine(this.PlayAudioClip(this.stageClearedSound, 0.6f));

        this.textKillCount.text = GameManager.instance.KillCount.ToString();
        if (GameManager.instance.KillCount > 0) 
            this.badKarmaBlock.SetActive(true);
        else 
            this.goodKarmaBlock.SetActive(true);

        this.stageClearedUI.SetActive(true);
        this.ShowEndingCount();
        this.isStageCleared = true;
    }

    /**
     * Pause bgm and play an audio clip, then resume the bgm
     * @param audioClip {AudioClip} The audio clip to play
     * @param relativeVolume {float} 1 = 100% bgm volume
     */
    private IEnumerator PlayAudioClip(AudioClip audioClip, float relativeVolume) {
        // Pause the bgm
        Camera.main.GetComponent<AudioSource>().Pause();

        if (this.audioSource.isPlaying)
            yield return null;
        else {
            this.audioSource.PlayOneShot(audioClip, relativeVolume * this.SfxVolume);
            yield return new WaitForSeconds(audioClip.length);
            Camera.main.GetComponent<AudioSource>().UnPause();
        }
    }

    private void ShowEndingCount() {
        this.textEndingsUnlocked.text = "You have unlocked " + Helper.CountUnlockedEndings() + "/4 endings.";
        this.textEndingsUnlocked.gameObject.SetActive(true);
    }
}
