using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour {
    public static BuildManager instance;
    public AudioClip spawningSound;
    public AudioClip mouseClickSound;
    public GameObject normalTurretPrefab;
    public GameObject magicTurretPrefab;
    public GameObject slowingTurretPrefab;

    public GameObject SelectedTurret {
        get { return this.selectedTurret; }
        set {
            this.selectedTurret = value;

            this.turretToBuild = null;
        }
    }

    public GameObject TurretToBuild {
        get { return this.turretToBuild; }
        set {
            if (!StageManager.instance.GameIsStarted)
                return;
            this.audioSource.PlayOneShot(this.mouseClickSound, 1f * StageManager.instance.SfxVolume);
            this.turretToBuild = value;
            TurretMenu.instance.Hide();

            this.selectedTurret = null;
        }
    }

    private AudioSource audioSource;
    private GameObject turretToBuild = null;
    private GameObject selectedTurret = null;

    void Awake () {
        if (instance != null) {
            Debug.LogError("More than one BuildManager in scene!");
            return;
        }
        instance = this;
    }

    void Start() {
        this.audioSource = Camera.main.GetComponent<AudioSource>();
    }

    public void Build(Vector3 location, WorldTile clickedTile) {
        if (this.TurretToBuild == null) 
            return;

        GameManager gameManager = GameManager.instance;
        int turretPrice = this.TurretToBuild.GetComponent<Turret>().BasePrice;

        if (!Helper.IsTooPoor(turretPrice)) {
            // cell to world return the upper left most position of the cell, we need the center.
            clickedTile.building = Instantiate(this.TurretToBuild, location, this.transform.rotation);
            
            this.audioSource.PlayOneShot(this.spawningSound, 0.7f * StageManager.instance.SfxVolume);

            gameManager.ManaCount -= turretPrice;
            gameManager.ManaSpent += turretPrice;
        } else 
            Debug.Log("You poor ass!");
    }

    public void SelectTurret(GameObject turret) {
        if (!StageManager.instance.GameIsStarted)
            return;

        this.turretToBuild = turret;
    }
}
