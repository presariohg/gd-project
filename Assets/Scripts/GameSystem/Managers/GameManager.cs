using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using static Constants;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public int startMana;

    public float restartDelay = 2f;

    public Text textKillCount;
    public Text textManaCount;
    public Text textManaSpent;

    public float ManaGainModifier { get; private set; }
    public float DamageOutputModifier { get; private set; }
    public float DamageInputModifier { get; private set; }

    public int KillCount {
        get { return this.killCount; }
        set { 
            this.killCount = value;
            this.textKillCount.text = value.ToString();
        }
    }

    public float ManaCount {
        get { return this.manaCount; }
        set {
            // Debug.Log(value);
            if (value > this.ManaCount) { // gaining
                float gainDiff = (value - this.manaCount) * this.ManaGainModifier;
                this.manaCount += gainDiff;
            } else
                this.manaCount = value;

            this.textManaCount.text = ((int) this.manaCount).ToString();
        }
    }

    public float ManaSpent {
        get { return this.manaSpent; }
        set {
            this.manaSpent = value;
            this.textManaSpent.text = ((int) value).ToString();
        }
    }

    private int killCount = 0;
    private float manaCount = 0;
    private float manaSpent = 0;
    [SerializeField] private float manaRegenPerSecond;

    void Awake() {
        if (instance != null) {
            Debug.LogError("More than one GameManager in scene!");
            return;
        }
        instance = this;
    }

    void Start() {
        this.manaCount = this.startMana;
        InvokeRepeating("PassiveRegenMana", 0, 1f); // each 1s
        this.ReadDifficulty();
    }

    public void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void PassiveRegenMana() {
        if (!Helper.TimeIsRunning())
            return;
        this.ManaCount += this.manaRegenPerSecond;
    }

    private void ReadDifficulty() {
        int difficulty = PlayerPrefs.GetInt(SettingKeys.DIFFICULTY, 0);
        this.ManaGainModifier = 1 - Mathf.Log(difficulty + 1, 9); // x1 at lv 0, x0.5 at lv 2
        this.DamageInputModifier = 1 + Mathf.Log(difficulty + 1, 9); // x1 at lv 0, x1.5 at lv 2
        Debug.Log(ManaGainModifier);
    }
}
