using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class WorldTile : ScriptableObject {
    public TileBase tileBase { get; set; }

    public GameObject building { get; set; }

    public Vector3 worldPosition { get; set; }
}