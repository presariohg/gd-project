using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants {
    public static readonly string TAG_ENEMY = "Enemy";
    public static readonly string TAG_HERO = "Hero";
    public static readonly string TAG_TURRET = "Turret";

    public enum DamageType {
        Physic,
        Magic,
        Soul, // penetrate
        Debuff,

        Mental,
    }

    public enum EffectType {
        Slow,
    }

    public enum FacingSide {
        Left,
        Right,
    }

    public enum DifficultySettings {
        Easy,
        Normal,
        Hard,
        Asian,
    }

    public static class SettingKeys {
        public static readonly string BGM_VOLUME = "bgm_volume";
        public static readonly string SFX_VOLUME = "sfx_volume";
        public static readonly string DIFFICULTY = "difficulty";
        public static readonly string GAME_OVER_BAD_KARMA = "game_over_bad_karma";
        public static readonly string GAME_OVER_GOOD_KARMA = "game_over_good_karma";
        public static readonly string BOSS_DEFEATED_BAD_KARMA = "stage_cleared_bad_karma";
        public static readonly string BOSS_DEFEATED_GOOD_KARMA = "stage_cleared_good_karma";

    }

    public static class MouseButton {
        public static readonly int LEFT = 0;
        public static readonly int RIGHT = 1;
        public static readonly int MIDDLE = 2;
    }

    public static class Stage1Dialogue {
        public const int OPENING = 0;
        public const int GAME_OVER_BAD_KARMA = 1;
        public const int GAME_OVER_GOOD_KARMA = 2;
        public const int BOSS_DEFEATED_BAD_KARMA = 3;
        public const int BOSS_DEFEATED_GOOD_KARMA = 4;
    }
}
