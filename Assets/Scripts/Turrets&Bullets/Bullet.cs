using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public class Bullet : MonoBehaviour {
    protected float speed;
    protected Transform target;
    protected float damageHealth;
    protected float damageMental;
    protected DamageType damageType;
    protected bool hasMercy;

    // Must call this at least once, right after instantiation
    public void Init(Transform target, float damageHealth, float damageMental, float speed, DamageType damageType, bool hasMercy=false) {
        this.target = target;
        this.damageHealth = damageHealth;
        this.damageMental = damageMental;
        this.speed = speed;
        this.damageType = damageType;
        this.hasMercy = hasMercy;
    }

    protected virtual void Update() {
        if (!Helper.TimeIsRunning())
            return;

        if (this.target == null) {
            Destroy(this.gameObject);
            return;
        }

        Vector3 dir = this.target.position - transform.position;
        float distanceThisFrame = this.speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame) {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
    }

    protected virtual void HitTarget() {
        Enemy target = this.target.gameObject.GetComponent<Enemy>();
        target.TakeDamage(damageHealth:this.damageHealth, 
                          damageMental:this.damageMental, 
                          damageType:this.damageType,
                          merciful:this.hasMercy);
        
        Destroy(gameObject);
    }
}
