using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletArrow : Bullet {
    private int OFFSET_Z = -45; // rotate on z axis by 45 degree to make it horizontal

    override protected void Update() {
        if (this.target == null) {
            Destroy(this.gameObject);
            return;
        }

        Vector3 movingDirection = this.target.transform.position - this.transform.position;
        float angle = Mathf.Atan2(movingDirection.y, movingDirection.x) * Mathf.Rad2Deg + OFFSET_Z;

        this.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        base.Update();
    }
}
