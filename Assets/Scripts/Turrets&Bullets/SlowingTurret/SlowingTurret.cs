using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public class SlowingTurret : Turret {
    [SerializeField] protected float effectDuration;
    [SerializeField] protected EffectType effectType;
    [SerializeField] protected float statModifier;

    private List<GameObject> enemiesInRange = new List<GameObject>();

    protected override void UpdateTarget() {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(Constants.TAG_ENEMY);
        this.enemiesInRange = new List<GameObject>();

        foreach (GameObject enemyGO in enemies) {
            if (enemyGO.GetComponent<Enemy>().IsFleeing)
                continue;

            float distanceToEnemy = Vector3.Distance(this.transform.position, enemyGO.transform.position);
            if (distanceToEnemy <= this.range) 
                this.enemiesInRange.Add(enemyGO);
        }
    }

    // AOE dmg
    protected override void Shoot() {
        if (this.enemiesInRange.Count == 0)
            return;

        GameObject nearestEnemy = this.enemiesInRange[0];
        float minDistance = Mathf.Infinity;

        foreach (GameObject enemyGO in this.enemiesInRange) {
            if (enemyGO.GetComponent<Enemy>().IsFleeing)
                continue;

            float distanceToEnemy = Vector3.Distance(this.transform.position, enemyGO.transform.position);
            if (distanceToEnemy < minDistance) {
                minDistance = distanceToEnemy;
                nearestEnemy = enemyGO;
            }

            float durationScale = (Mathf.Log(this.CurrentLevel, 2) + 1); // x1 at level 1

            Enemy enemy = enemyGO.GetComponent<Enemy>();
            enemy.TakeEffect(effectType:this.effectType,
                             duration:this.effectDuration * durationScale,
                             modifier:this.statModifier);
        }

        this.facingSide = Helper.CheckFlipSprite(this.gameObject, nearestEnemy.transform, this.facingSide);

        this.fireCountdown = 1 / fireRate;
    }

}