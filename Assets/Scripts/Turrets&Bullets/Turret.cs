using System.Collections;
using UnityEngine;
using static Constants;

public abstract class Turret : MonoBehaviour {
    public AudioClip shootingSound;
    public AudioClip upgradingSound;

    [SerializeField] protected float range;
    [SerializeField] protected float fireRate;
    [SerializeField] protected float bulletSpeed;
    [SerializeField] protected int baseCost; // increase with a modifier as the stage count increase
    [SerializeField] protected int maxLevel;

    [SerializeField] protected int baseDamageHealth;
    [SerializeField] protected int baseDamageMental;

    [SerializeField] protected DamageType damageType;
    [SerializeField] protected string turretName;

    [SerializeField] protected FacingSide facingSide;

    public GameObject bullet;
    public string Name { get {return this.turretName; }}
    public float Range { get {return this.range; }}
    public int MaxLevel { get {return this.maxLevel; }}
    public int CurrentLevel { get; private set; }
    public int BuyPrice { get {return this.baseCost * this.CurrentLevel; }}
    public int SellPrice { get {return this.BuyPrice / 2; }}
    public int BasePrice { get {return this.baseCost; }}
    public bool HasMercy{ get; set; }

    protected float fireCountdown;
    private AudioSource audioSource;
    private Animator animator;
    private Transform target = null;
    private Vector3 bulletScale;
    private float damageScale;

    void Start() {
        this.audioSource = Camera.main.GetComponent<AudioSource>();
        this.animator = this.gameObject.GetComponent<Animator>();
        this.animator.ResetTrigger("LvUp");
        this.bulletScale = this.bullet.transform.localScale;
        this.damageScale = 1;
        this.HasMercy = GameObject.FindGameObjectWithTag(Constants.TAG_HERO).GetComponent<Hero>().HasMercy;

        InvokeRepeating("UpdateTarget", 0, 1 / (2 * this.fireRate)); // must reset invoking if fire rate changed
    }

    void Awake() {
        this.CurrentLevel = 1;
    }

    void Update() {
        if (!Helper.TimeIsRunning())
            return;

        // if (this.target == null) 
        //     return;

        if (this.fireCountdown <= 0)
            Shoot();
            
        this.fireCountdown -= Time.deltaTime;
    }

    public void Upgrade() {
        GameManager gameManager = GameManager.instance;
        int priceBuying = this.BuyPrice;

        if ((this.CurrentLevel >= this.maxLevel) ||
            (priceBuying > gameManager.ManaCount))
            return;

        this.audioSource.PlayOneShot(this.upgradingSound, 0.7f * StageManager.instance.SfxVolume);
        this.CurrentLevel++;
        this.bulletScale = this.bullet.transform.localScale * (Mathf.Log(this.CurrentLevel, 3) + 1); // make the bullets bigger, max = 2x
        this.damageScale = (Mathf.Log(this.CurrentLevel, 3) + 1);
        this.animator.ResetTrigger("LvUp");
        this.animator.SetTrigger("LvUp");
        gameManager.ManaCount -= priceBuying;
        gameManager.ManaSpent += priceBuying;
    }

    public void Sell() {
        GameManager gameManager = GameManager.instance;
        gameManager.ManaCount += this.SellPrice;
        Destroy(gameObject);
    }

    protected virtual void Shoot() {
        if (this.target == null) 
            return;

        this.facingSide = Helper.CheckFlipSprite(this.gameObject, this.target, this.facingSide);

        GameObject bulletGO = (GameObject) Instantiate(this.bullet, this.transform.position, this.transform.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();

        // increase bullet size + damage with this turret's level
        bulletGO.transform.localScale = this.bulletScale;

        if (bullet != null) {
            this.audioSource.PlayOneShot(this.shootingSound, 0.5f * StageManager.instance.SfxVolume); // 0.5x bgm
            bullet.Init(target              : this.target,
                        damageHealth        : this.baseDamageHealth * this.damageScale,
                        damageMental        : this.baseDamageMental * this.damageScale,
                        speed               : this.bulletSpeed,
                        damageType          : this.damageType,
                        hasMercy            : this.HasMercy);
        }

        this.fireCountdown = 1 / fireRate;
    }

    protected virtual void UpdateTarget() {
        if (!Helper.TimeIsRunning())
            return;

        GameObject[] enemies = GameObject.FindGameObjectsWithTag(Constants.TAG_ENEMY);

        float minDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemyGO in enemies) {
            Enemy enemy = enemyGO.GetComponent<Enemy>();
            if (enemy.IsFleeing && this.HasMercy)
                continue; // skip fleeing enemies

            float distanceToEnemy = Vector3.Distance(transform.position, enemyGO.transform.position);
            if (distanceToEnemy < minDistance) {
                minDistance = distanceToEnemy;
                nearestEnemy = enemyGO;
            }
        }

        if (nearestEnemy != null && minDistance <= this.range)
            this.target = nearestEnemy.transform;
        else
            this.target = null;
    }

    protected bool oneHitKO(Enemy enemy) {
        return this.baseDamageHealth * this.damageScale >= enemy.HP;
    }
}
