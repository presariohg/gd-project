using UnityEngine;
using static Constants;
using System.Collections.Generic;

public class EnemyFrederick : Enemy {
    public override Dictionary<DamageType, float> DamageModifier 

        => new Dictionary<DamageType, float>() {
            {DamageType.Physic, 0.08f},
            {DamageType.Magic, 0.08f},
            {DamageType.Debuff, 0.5f},
        };

    public override void TakeDamage(float damageHealth, float damageMental, DamageType damageType, bool merciful=false) {
        // check if this damage should be reduced
        float hpModifier, mpModifier;

        if (this.DamageModifier.TryGetValue(damageType, out hpModifier)) 
            damageHealth *= hpModifier;

        if (this.DamageModifier.TryGetValue(DamageType.Mental, out mpModifier))
            // or some enemies can have their own mental dmg modifier
            damageMental *= mpModifier;
        else
            if (hpModifier != 0)
                // or by default mental dmg is scaled with health dmg
                damageMental *= hpModifier;

        this.HP -= damageHealth;
        // this.MP -= damageMental;

        GameManager.instance.ManaCount += GetManaByDamage(damageHealth);

        if ((this.healthPoint <= 0) && !this.IsFleeing) {
            if (GameManager.instance.KillCount == 0) 
                DialogueManager.instance.StartDialogue(Stage1Dialogue.BOSS_DEFEATED_GOOD_KARMA);
            else 
                DialogueManager.instance.StartDialogue(Stage1Dialogue.BOSS_DEFEATED_BAD_KARMA);

            if (merciful)
                this.IsFleeing = true;
            else
                this.Die();
        }

        if ((!this.IsFleeing) && (this.HP <= 0))  // Flee
            this.IsFleeing = true;

        return;
    }        
}