using UnityEngine;
using static Constants;
using System.Collections.Generic;

public class EnemyArmored : Enemy {
    public override Dictionary<DamageType, float> DamageModifier 

        => new Dictionary<DamageType, float>() {
            {DamageType.Physic, 0.125f}, 
            {DamageType.Magic, 0.75f}
        };
}