using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public abstract class Character : MonoBehaviour {
    public HealthBar healthBar;

    public float HP {
        get { return this.healthPoint; }
        protected set {
            this.healthPoint = value;
            this.healthBar.SetHealth((int) value);
        }
    }
    protected float healthPoint = 100;

    /**
     * @param {int} damage: reduce HP by this amount
     *
     * By default one unit cannot be dead. (this function doesn't call Die()).
     * Only die-able if the derived class implement this and call Die()
     */
    public virtual void TakeDamage(float damage) { // Assumed that all damage was converted into physical
        this.HP -= damage;
    }

    public virtual IEnumerator Die() {
        while (!Helper.TimeIsRunning()) // only destroy object when time is running
            yield return new WaitForSeconds(0.5f);

        Destroy(gameObject);
    }
}
