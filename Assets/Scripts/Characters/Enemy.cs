using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public class Enemy : Character {
    public HealthBar mentalBar;

    public virtual float Reward { get { return this.reward; } }
    public virtual int Damage { get { return this.damageToHero; } }

    public virtual Dictionary<DamageType, float> DamageModifier {
        get { return new Dictionary<DamageType, float>(); }
    }

    public float MP { 
        get { return this.mentalHealth; } 
        protected set {
            this.mentalHealth = value;
            this.mentalBar.SetHealth((int) value);            
        }
    }
    
    public bool IsFleeing { 
        get {return this.isFleeing; }
        protected set {
            if (value)
                this.speed *= 1.5f;
            this.isFleeing = value;
            this.RefreshTarget();
        }
    }

    [SerializeField] private float speed;
    [SerializeField] private float reward;
    [SerializeField] private int damageToHero;
    [SerializeField] private FacingSide facingSide;
    protected Transform target;
    protected int wavepointIndex = 0;
    protected float mentalHealth = 100f;
    protected bool isFleeing;


    // Should change into pathfinding later, instead of using fixed waypoints like this
    void Start() {
        this.target = Waypoints.points[0];
        this.IsFleeing = false;
    }

    void Update() {
        if (!Helper.TimeIsRunning())
            return;

        Vector3 dir = this.target.position - this.transform.position;

        if (this.speed > 0)
            this.transform.Translate(dir.normalized * this.speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, this.target.position) < 0.2f) {
            this.RefreshTarget();
        }
    }

    /**
     * Call this to inflict both mental and health damage. Call TakeDamage(1 args) will only damage health
     * @param {int} damage Reduce HP by this number
     * @param {int} damageMental Reduce mental by this number, modifier applied
     * @param {DamageType} damageType Duh, just damage type
     */
    public virtual void TakeDamage(float damageHealth, float damageMental, DamageType damageType, bool merciful=false) {
        // check if this damage should be reduced
        float hpModifier, mpModifier;

        if (this.DamageModifier.TryGetValue(damageType, out hpModifier)) 
            damageHealth *= hpModifier;

        if (this.DamageModifier.TryGetValue(DamageType.Mental, out mpModifier))
            // or some enemies can have their own mental dmg modifier
            damageMental *= mpModifier;
        else
            if (hpModifier != 0)
                // or by default mental dmg is scaled with health dmg
                damageMental *= hpModifier;

        this.HP -= damageHealth;
        // this.MP -= damageMental;

        GameManager.instance.ManaCount += GetManaByDamage(damageHealth);

        if (this.healthPoint <= 0)
            if (merciful)
                // this.MP = 0; 
                this.IsFleeing = true;
            else
                StartCoroutine(this.Die());

        if ((!this.IsFleeing) && (this.HP <= 0))  // Flee
            this.IsFleeing = true;

        return;
    }

    public void TakeEffect(EffectType effectType, float duration=0, float modifier=0) {
        float debuffModifier;

        if (this.DamageModifier.TryGetValue(DamageType.Debuff, out debuffModifier)) 
            modifier *= debuffModifier; // reduce debuff effect

        if (effectType == EffectType.Slow) {
            StartCoroutine(this.ChangeSpeed(duration, modifier));
        }
    }

    public float GetManaByDamage(float damage) {
        // 75% reward by reducing enemy HP to 0, 25% reward by killing enemy
        // each enemy has a fixed 100 HP
        return (damage * (0.75f * this.reward / 100f)); 
    }

    public override IEnumerator Die() {
        GameManager gameManager = GameManager.instance;

        // 75% reward by reducing enemy HP to 0, 25% reward by killing enemy
        gameManager.ManaCount += 0.25f * this.reward;
        gameManager.KillCount++;

        return base.Die();
    }

    private IEnumerator ChangeSpeed(float duration, float modifier) {
        this.speed *= modifier;
        yield return new WaitForSeconds(duration);
        this.speed /= modifier;
    }

    protected void RefreshTarget() {
        if (this.IsFleeing)
            if (this.wavepointIndex == 0) {
                Destroy(this.gameObject); // fled successfully
                return;
            } else {
                this.wavepointIndex--;
                this.target = Waypoints.points[this.wavepointIndex];
            }
        else
            if (this.wavepointIndex >= Waypoints.points.Length - 1)
                // Will be destroyed if the hero is still alive, see Hero.update()
                return;
            else {
                this.wavepointIndex++;
                this.target = Waypoints.points[this.wavepointIndex];
            }

        this.facingSide = Helper.CheckFlipSprite(this.gameObject, this.target, this.facingSide);
    }
}