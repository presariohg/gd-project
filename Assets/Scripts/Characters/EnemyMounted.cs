using UnityEngine;
using static Constants;
using System.Collections.Generic;

public class EnemyMounted : Enemy {
    public override Dictionary<DamageType, float> DamageModifier 

        => new Dictionary<DamageType, float>() {
            {DamageType.Physic, 0.5f}
        };
}