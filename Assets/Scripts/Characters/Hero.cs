using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Constants;

public class Hero : Character {
    public GameObject heroMenu;
    public Slider healingSlider;

    [SerializeField] private Grid grid;

    public bool HasMercy { 
        get { return this.hasMercy; }
        private set {
            this.hasMercy = value;

            if (this.toggleMercy.isOn != value)
                this.toggleMercy.isOn = value;

            GameObject[] turretsGO = GameObject.FindGameObjectsWithTag(Constants.TAG_TURRET);
            foreach (GameObject turretGO in turretsGO) {
                Turret turret = turretGO.GetComponent<Turret>();
                turret.HasMercy = value;
            }
        } 
    }
    public Toggle toggleMercy;

    // TODO: Dynamic healing price
    private Vector3Int currentGridPosition;
    private float healingDuration = 2.5f; // healing complete in second
    private float healingRefreshRate = 0.1f; // refresh HP each n seconds
    private bool isHealing; // don't stack multiple healing command at once
    private bool menuButtonsClicked; // don't close the menu after clicking a hero menu button
    private bool hasMercy;

    void Start() {
        this.currentGridPosition = grid.WorldToCell(transform.position);
        this.menuButtonsClicked = false;
        this.HasMercy = false;
    }

    void Update() {
        if (Input.GetMouseButtonUp(MouseButton.LEFT))
            this.OnMouseClick();

        if (!Helper.TimeIsRunning()) 
            return;

        GameObject nearestEnemy = Helper.GetNearestEnemy(center:this.transform);

        if (nearestEnemy != null) {
            Vector3Int enemyGridPosition = this.grid.WorldToCell(nearestEnemy.transform.position);
            if (enemyGridPosition == this.currentGridPosition) {
                Enemy enemy = nearestEnemy.GetComponent<Enemy>();

                float damageOnHero = enemy.Damage * (enemy.HP / 100) * GameManager.instance.DamageInputModifier; // weakened enemies deal less dmg
                float damageOnEnemy = enemy.HP;

                if (damageOnHero >= this.HP) 
                    damageOnEnemy -= 1; // enemy survives

                enemy.TakeDamage(damageOnEnemy, enemy.MP, DamageType.Soul, merciful:this.HasMercy);
                this.TakeDamage(damage:damageOnHero);
            }
        }
    }

    void OnMouseClick() {
        Vector3Int clickedTilePos = Helper.GetMouseTile(this.grid);
        if (clickedTilePos == this.currentGridPosition)
            this.heroMenu.SetActive(!this.heroMenu.activeSelf);
        else if (this.menuButtonsClicked)
            this.menuButtonsClicked = false;
        else
            this.heroMenu.SetActive(false);
    }

    public void ToggleMercy() {
        this.menuButtonsClicked = true;
        this.HasMercy = this.toggleMercy.isOn;
    }

    public void Heal10Up() {
        this.menuButtonsClicked = true;

        if ((isHealing) ||
            (Helper.IsTooPoor(10)) ||
            (this.HP >= 100))
            return;

        // disable heal button if hp full

        float goalHP = Mathf.Min(100, this.HP + 10); // amount after healing
        float healingAmount = 10f * healingRefreshRate / healingDuration; // amount each invocation

        this.healingSlider.value = goalHP;
        GameManager.instance.ManaCount -= 10;
        StartCoroutine(this.Heal(healingAmount, goalHP));
    }
    
    private IEnumerator Heal(float healingAmount, float goalHP) {
        do  {
            yield return new WaitForSeconds(this.healingRefreshRate);
            this.HP += healingAmount;
        } while (this.HP < goalHP);
        this.healingSlider.value = 0;
    }


    public override void TakeDamage(float damage) {
        this.HP -= damage;
        if (this.HP <= 0)
            this.Die();
        
        return;
    }

    public override IEnumerator Die() {
        StageManager.instance.GameOver();
        return base.Die();
    }
}
