using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretMenu : MonoBehaviour {
    public static TurretMenu instance;

    public GameObject ui;
    public GameObject upgradeButton;
    public Transform turretRange;

    public Text levelText;
    public Text upgradeCostText;
    public Text sellValueText;


    private Vector3 turretRangeBaseScale;

    public GameObject Target {
        get {return this.target;}
        set {
            Turret selectedTurret = value.GetComponent<Turret>();

            // hide upgrade button if current level is max
            if (selectedTurret.CurrentLevel == selectedTurret.MaxLevel)
                this.upgradeButton.SetActive(false);
            else 
                this.upgradeButton.SetActive(true);

            this.target = value;
            this.transform.position = this.target.transform.position + new Vector3(0f, 1.5f, 0f); // hover up a bit

            this.levelText.text         = selectedTurret.Name + " Lv. " + selectedTurret.CurrentLevel.ToString();
            this.upgradeCostText.text   = selectedTurret.BuyPrice.ToString();
            this.sellValueText.text     = selectedTurret.SellPrice.ToString();
            this.turretRange.localScale = selectedTurret.Range * this.turretRangeBaseScale;

            this.ui.SetActive(true);
        }
    }

    private GameObject target = null;

    void Awake () {
        if (instance != null) {
            Debug.LogError("More than one TurretMenu in scene!");
            return;
        }
        instance = this;
    }

    void Start() {
        this.Hide();
        this.turretRangeBaseScale = this.turretRange.localScale;
    }

    public void UpgradeTarget() {
        MapManager.instance.TurretMenuClicked = true;
        this.target.GetComponent<Turret>().Upgrade();
    }

    public void SellTarget() {
        MapManager.instance.TurretMenuClicked = true;
        this.target.GetComponent<Turret>().Sell();
    }

    public void Hide() {
        this.ui.SetActive(false);
    }

    public bool IsActive() {
        return this.ui.activeSelf;
    }
}
