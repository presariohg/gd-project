using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    public GameObject chapterSelectionUI;
    public GameObject settingsUI;
    public GameObject creditsUI;

    private string defaultLevel = "Level0";

    void Update() {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            this.chapterSelectionUI.SetActive(false);
        }
    }

    public void Play() {
        SceneManager.LoadScene(this.defaultLevel);
    }

    public void LauchSettings() {
        this.settingsUI.SetActive(true);
    }

    public void ToggleCredits() {
        this.creditsUI.SetActive(!this.creditsUI.activeSelf);
    }

    public void ToggleSelectChapter() {
        this.chapterSelectionUI.SetActive(!this.chapterSelectionUI.activeSelf);
    }

    public void Quit() {
        Debug.Log("Exit!");
        Application.Quit();
    }
}
