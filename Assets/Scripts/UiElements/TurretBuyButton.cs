using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretBuyButton : MonoBehaviour {
    public Text textPrice;
    public Turret turret;

    void Start() {
        this.textPrice.text = turret.BasePrice.ToString();
    }
}
