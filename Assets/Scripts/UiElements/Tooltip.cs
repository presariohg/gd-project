using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    public GameObject tooltipText;
    public void OnPointerEnter(PointerEventData eventData) {
        this.tooltipText.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData) {
        this.tooltipText.SetActive(false);
    }
}
