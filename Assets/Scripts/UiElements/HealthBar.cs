using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
    public Slider slider;
    public Gradient gradient;
    public Image fill;
    public Image icon;

    protected float iconOpacity;

    void Start() {
        this.fill.color = this.gradient.Evaluate(this.slider.normalizedValue);
        this.iconOpacity = this.icon.color.a;
        this.gameObject.SetActive(false);
    }

    public void SetHealth(int health) {
        this.slider.value = health;
        // if (this.slider.normalizedValue < 1 && this.slider.normalizedValue > 0)
        if (this.slider.normalizedValue < 1)
            this.gameObject.SetActive(true);

        Color color = this.gradient.Evaluate(this.slider.normalizedValue);
        this.fill.color = color;

        // keep opacity
        color.a = this.iconOpacity;
        this.icon.color = color;
    }

}
