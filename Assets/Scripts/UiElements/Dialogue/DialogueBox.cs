using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBox : MonoBehaviour {
    public Text speakerName;
    public Text sentenceContent;
    public Image speakerAvatar;
    public Image dialogueBox;
    public Image continueCursor;
}
