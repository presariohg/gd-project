using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Sentence {
    public string speakerName;
    [TextArea(3, 5)] public string content;
    public Sprite speakerAvatar;
    public Sprite dialogueBox;
    public bool onTheLeft;
}
