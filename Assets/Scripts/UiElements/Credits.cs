using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour {
    public GameObject creditsUI;

    public void CloseWindow() {
        this.creditsUI.SetActive(false);
    }
}
