using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Constants;

public class Settings : MonoBehaviour {
    public static Settings instance;

    public Text textEndingsUnlocked;

    public Dropdown difficultyDropdown;
    public Slider sfxSlider;
    public Slider bgmSlider;
    public AudioSource audioSource;

    public int Difficulty { 
        get { return this.difficultyValue; }
        private set {
            this.difficultyValue = value;
            this.difficultyDropdown.value = value;
        }
    }
    public float BgmVolume {
        get { return this.bgmVolume; }
        private set {
            this.bgmVolume = value;
            this.bgmSlider.value = value;
        } 
    }

    public float SfxVolume {
        get { return this.sfxVolume; }
        private set {
            this.sfxVolume = value;
            this.sfxSlider.value = value;
        }
    }

    protected class TempSettings {
        public static float bgmVolume;
        public static float sfxVolume;
        public static int difficultyValue;
    }

    private int difficultyValue;
    private float bgmVolume;
    private float sfxVolume;

    public static readonly DifficultySettings[] difficulties = {DifficultySettings.Easy, 
                                                                DifficultySettings.Normal, 
                                                                DifficultySettings.Hard, 
                                                                DifficultySettings.Asian};
    void Awake() {
        if (instance != null) {
            Debug.LogError("More than one Settings UI in scene!");
            return;
        }
        instance = this;
        this.textEndingsUnlocked.text = "You have unlocked " + Helper.CountUnlockedEndings() + "/4 endings";
    }

    void Start() {
        this.BgmVolume = PlayerPrefs.GetFloat(SettingKeys.BGM_VOLUME, 1f);
        this.SfxVolume = PlayerPrefs.GetFloat(SettingKeys.SFX_VOLUME, 1f);
        this.Difficulty = PlayerPrefs.GetInt(SettingKeys.DIFFICULTY, 0);
        this.difficultyDropdown.value = this.difficultyValue;

        // Debug.Log(this.bgmSlider.value);
        // Debug.Log(this.sfxSlider.value);
    }


    public void ChangeDifficulty() {
        TempSettings.difficultyValue = this.difficultyDropdown.value;
    }

    public void ChangeBgmVolume() {
        TempSettings.bgmVolume = this.bgmSlider.value;
        this.audioSource.volume = TempSettings.bgmVolume;
    }

    public void ChangeSfxVolume() {
        TempSettings.sfxVolume = this.sfxSlider.value;

    }

    public void ClearSavedData() {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
        this.textEndingsUnlocked.text = "You have unlocked 0/4 endings";
    }

    public void Save() {
        this.Difficulty = TempSettings.difficultyValue;
        this.BgmVolume = TempSettings.bgmVolume;
        this.SfxVolume = TempSettings.sfxVolume;

        PlayerPrefs.SetFloat(SettingKeys.BGM_VOLUME, this.bgmSlider.value);
        PlayerPrefs.SetFloat(SettingKeys.SFX_VOLUME, this.sfxSlider.value);
        PlayerPrefs.SetInt(SettingKeys.DIFFICULTY, this.difficultyValue);
        PlayerPrefs.Save();

        this.gameObject.SetActive(false);
    }

    public void Cancel() {
        // unset bgm volume + all gui elems
        
        this.audioSource.volume = this.BgmVolume; 
        this.bgmSlider.value = this.BgmVolume;
        this.sfxSlider.value = this.SfxVolume;
        this.difficultyDropdown.value = this.Difficulty;


        this.gameObject.SetActive(false);
    }
}
